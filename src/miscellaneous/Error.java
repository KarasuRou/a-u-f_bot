package miscellaneous;

import net.dv8tion.jda.api.events.GenericEvent;

public class Error {
	
	public static void fatal() {
		
	}

	public static void unknown(String text) {
		System.out.println(text);
	}
	public static void unknown(GenericEvent event) {
		System.out.println(event);
	}
	public static void unknown(String text, GenericEvent event) {
		System.out.println(text + event);
	}
}
