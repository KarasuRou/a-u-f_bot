package console;

import master.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.dv8tion.jda.api.JDA;

public class Loop {
	private static JDA jda = Master.jdaBot;

	@SuppressWarnings("all")
	public static void Loop() {
		new Thread(()->{
			String line = "";
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			try {
				while((line = reader.readLine()) != null) {
					if (Check(line, "shutdown")) {
						Shutdown();
					}
					else if (Check(line, "restart")) {
						Restart();
					}
					else if (Check(line, "whoami")) {
						System.out.println(jda.getSelfUser());
					}
					else if (Check(line, "ping")) {
						System.out.println(jda.getGatewayPing());
					}
					else if (Check(line, "status")) {
						System.out.println(jda.getStatus());
					}
					else if (Check(line, "guilds")) {
						System.out.println(jda.getGuilds());
					}
					else if (Check(line, "textchannels")) {
						System.out.println(jda.getTextChannels());
					}
					else if (Check(line, "privatetext")) {
						System.out.println(jda.getPrivateChannels());
					}
					else if (Check(line, "write guilds")) {
						WriteGuilds();
					}
					else if (Check(line, "ban")) {
						jda.getGuildById("809861030774571010").ban("530419485136650250", 1, "Blub").complete();
					}
					else { 
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	private static void WriteGuilds() {
		jda.getTextChannels().get(0).sendMessage("Test").queue();
	}


	private static void Shutdown() {
		jda.shutdown();
	}
	
	private static void Restart() throws Exception{
		jda.shutdown();
		Thread.sleep(5000);
		Master.CreateBot();
	}
	
	private static boolean Check(String check, String checked) {
		if (check.equalsIgnoreCase(checked)) 
			return true;
		return false;
	}
}


//private static void Spam() throws Exception{
//	String[] strings = {"530419485136650250","530419391842484256","331783703392944128"};
//	for (String string : strings) {
//
//
//		jda.openPrivateChannelById(string).complete();
//		Thread.sleep(500);
//		List<PrivateChannel> test = Master.jdaBot.getPrivateChannels();
//		int i = 0;
//		for (PrivateChannel privateChannel : test) {
//			if (privateChannel.getUser().getId().equals(string)) {
//				int y = 0;
//				do {
//					jda.getPrivateChannelById(privateChannel.getId()).sendMessage(String.valueOf(++y)).queue();
//				}while(y!=20);
//			}
//		}
//	}
//}