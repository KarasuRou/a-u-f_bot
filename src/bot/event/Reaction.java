package bot.event;

import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;

public class Reaction{

	public static void onGuildMessageReactionAddEvent(GuildMessageReactionAddEvent event) {
		System.out.println("onGuildMessageReactionAddEvent");
	}

	public static void onGuildMessageReactionRemoveEvent(GuildMessageReactionRemoveEvent event) {
		System.out.println("onGuildMessageReactionRemoveEvent");
	}

}
