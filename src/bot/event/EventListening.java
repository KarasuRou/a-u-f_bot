package bot.event;

import miscellaneous.Error;
import net.dv8tion.jda.api.events.GatewayPingEvent;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.ShutdownEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;

public class EventListening implements EventListener{
	
	@Override
	public void onEvent(GenericEvent event) {
		try {
			if (event instanceof ShutdownEvent) {
				Shutdown_Ready.onShutdownEvent((ShutdownEvent) event);
			}
			else if (event instanceof ReadyEvent) {
				Shutdown_Ready.onReadyEvent((ReadyEvent) event);
			}
			else if (event instanceof GuildMemberJoinEvent) {
				Leave_Join.onGuildMemberJoinEvent((GuildMemberJoinEvent) event);
			}
			else if (event instanceof GuildMemberRemoveEvent) {
				Leave_Join.onGuildMemberRemoveEvent((GuildMemberRemoveEvent) event);
			}
			else if (event instanceof GuildMessageReceivedEvent) {
				MessageRecieved.onGuildMessageRevievedEvent((GuildMessageReceivedEvent) event);
			}
			else if (event instanceof PrivateMessageReceivedEvent) {
				MessageRecieved.onPrivateMessageReceivedEvent((PrivateMessageReceivedEvent) event);
			}
			else if (event instanceof GuildMessageReactionAddEvent) {
				Reaction.onGuildMessageReactionAddEvent((GuildMessageReactionAddEvent) event);
			}
			else if (event instanceof GuildMessageReactionRemoveEvent) {
				Reaction.onGuildMessageReactionRemoveEvent((GuildMessageReactionRemoveEvent) event);
			}
			else if (event instanceof GatewayPingEvent) {
				if (((GatewayPingEvent) event).getNewPing() >= 150)
					System.err.println("Ping: " + ((GatewayPingEvent) event).getNewPing());
				else 
					System.out.println("Ping: " + ((GatewayPingEvent) event).getNewPing());
			}
			else {
	//			System.out.println("KP: " + event);
			}
		} catch (Exception e) {
			Error.unknown(event);
		}
		
	}
}
