package master;

import bot.event.EventListening;
import console.Loop;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.hooks.InterfacedEventManager;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;

public class Master{

	public static InterfacedEventManager eventManager = new InterfacedEventManager();
	public static JDABuilder botBuilder;
	
	public static String command_prefix = "!";
	public static Boolean debugmode = true;
	public static Boolean show_message = false;
	public static JDA jdaBot;
	
	public static void main(String[] args) {
		try {
			CreateBot();
			Loop.Loop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void CreateBot() throws Exception{
		botBuilder = JDABuilder.create(
				GatewayIntent.GUILD_MEMBERS,
				GatewayIntent.GUILD_PRESENCES,
				GatewayIntent.GUILD_MESSAGES,
				GatewayIntent.DIRECT_MESSAGES,
				GatewayIntent.GUILD_MESSAGE_REACTIONS,
				GatewayIntent.GUILD_EMOJIS,
				GatewayIntent.GUILD_VOICE_STATES);
		botBuilder.setMemberCachePolicy(MemberCachePolicy.ALL);
		botBuilder.addEventListeners(new EventListening());
		botBuilder.setActivity(Activity.listening("I am Online"));
		botBuilder.setStatus(OnlineStatus.ONLINE);
		jdaBot = botBuilder.build();
	}
}
